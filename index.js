'use strict';

(function () {
  var grydValidator = require('gryd-validator');
  var express       = require('express');
  var bunyan        = require('bunyan');
  var thinky        = require('thinky');
  var fs            = require('fs');

  var God = function God(env) {
    var self         = this;
    self.BASE_DIR    = __dirname + '/src';
    self.log         = bunyan.createLogger({name: 'GodProcess'});
    self.ENV         = env || process.env.NODE_ENV || 'local';
    self.express     = express;
    self.config      = {};
    self.controllers = {};
    self.models      = {};
    self.utils       = {};
    self.errorCodes  = {};

    self.log.info('Initiating God service');
    self.loadConfig();
    self.injectAppFeatures();
    self.loadLocales();
  };

  God.prototype.start = function start() {
    var self    = this;
    self.app    = express();
    self.db     = thinky(self.config.thinky);
    self.routes = require(self.BASE_DIR + '/routes.js');
    self.app.listen(self.config.port);
    self.log.info('God service started on port', self.config.port);
  };

  God.prototype.controller = function controller(key) {
    var self = this;
    if (!self.controllers[key]) {
      self.controllers[key] = require(self.BASE_DIR + '/controllers/' + key);
    }
    return self.controllers[key];
  };

  God.prototype.model = function model(key) {
    var self = this;
    if (!self.models[key]) {
      self.models[key] = require(self.BASE_DIR + '/models/' + key);
    }
    return self.models[key];
  };

  God.prototype.util = function util(key) {
    var self = this;
    if (!self.utils[key]) {
      self.utils[key] = require(self.BASE_DIR + '/utils/' + key);
    }
    return self.utils[key];
  };

  God.prototype.injectAppFeatures = function injectAppFeatures() {
    var self = this;
    grydValidator(self.express);
  };

  God.prototype.loadConfig = function loadConfig() {
    var self = this;
    switch (self.ENV) {
      case 'development':
      case 'dev':
        self.ENV = 'dev';
        break;
      case 'production':
      case 'prod':
        self.ENV = 'prod';
        break;
      default:
        self.ENV = 'local';
        break;
    }
    self.config = require(self.BASE_DIR + '/config/' + self.ENV);
  };

  God.prototype.loadLocales = function loadLocales() {
    var self = this;
    fs.readdirSync(self.BASE_DIR + '/locales').forEach(function (file) {
      if (file.match(/\.js$/) !== null && file !== 'index.js') {
        var name              = file.replace('.js', '');
        self.errorCodes[name] = require(self.BASE_DIR + '/locales/' + file);
      }
    });
  };

  module.god = new God();
  module.god.start();
})();