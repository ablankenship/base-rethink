var assert = require('assert');
var mocha = require('mocha');

describe('Testing God Process', function(){
  require(__dirname + '/../index.js');
  it('Should return a module with `god` property', function(done){

    var pass = false;
    for(var i in module.children){
      if(module.children[i].god){
        pass = true;
      }
    }
    assert.ok(pass, 'Module does not contain `god` property');
    done();
  });
});