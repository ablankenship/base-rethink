'use strict';

(function (god) {
  var locale     = require('locale');
  var bodyParser = require('body-parser');
  var log        = god.log.child({file: 'routes'});
  var Middleware = god.util('middleware');
  var app        = god.app;

  app.use(locale(Object.keys(god.errorCodes)));
  app.use(Middleware.locales);
  app.use(bodyParser.urlencoded({extended: true}));
  app.use(bodyParser.json());

  //Users routes
  var Users = god.controller('users');
  app.get('/v1/users', Users.getUserList);
  app.get('/v1/users/:id', Users.getUserById);
  app.post('/v1/users', Users.postCreateUser);
  app.put('/v1/users/:id', Users.putUpdateUser);
  app.delete('/v1/users/:id', Users.deleteUser);

  /* Final Middleware - 404 + Error Handler */
  app.use(function (req, res, next) {
    res.sendError('NOT_FOUND');
  });
  app.use(function (err, req, res, next) {
    if (err) {
      log.error('Unknown Error',err);
      res.sendError('UNKNOWN_ERROR');
    } else {
      next();
    }
  });
})(module.parent.god);

