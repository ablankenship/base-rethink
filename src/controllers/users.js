'use strict';
(function (god) {
  var log     = god.log.child({controller: 'users'});
  var Account = god.model('account');
  var User    = god.model('user');

  exports.getUserList = function getUserList(req, res) {
    res.success();
  };

  exports.getUserById = function getUserById(req, res) {
    res.success();
  };

  exports.postCreateUser = function postCreateUser(req, res) {
    res.success();
  };

  exports.putUpdateUser = function putUpdateUser(req, res) {
    res.success();
  };

  exports.deleteUser = function deleteUser(req, res) {
    res.success();
  };

})(module.parent.god);

