'use strict';
(function (god) {
  var log     = god.log.child({model: 'account'});
  var dbfuncs = god.util('dbfuncs');
  var db      = god.db;
  var type    = db.type;

  var Account = module.exports = db.createModel('Account', {
    name: type
      .string()
      .required(),
    status: type
      .string()
      .enum(['inactive', 'active', 'disabled']),
    created: type
      .date()
      .default(dbfuncs.generateDate),
    updated: type
      .date()
      .default(null)
  });
  module.exports = Account;
  Account.pre('save', dbfuncs.setUpdatedDate);

  var User = god.model('user');
  Account.hasMany(User, 'users', 'id', 'accountId');
})(module.parent.god);

