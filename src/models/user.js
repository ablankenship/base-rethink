'use strict';

(function (god) {
  var log     = god.log.child({model: 'user'});
  var dbfuncs = god.util('dbfuncs');
  var db      = god.db;
  var type    = db.type;

  var User = module.exports = db.createModel('User', {
    accountId: type
      .string()
      .required(),
    firstName: type
      .string()
      .required(),
    lastName: type
      .string()
      .required(),
    email: type
      .string()
      .email(),
    password: type
      .string(),
    google: type
      .object(),
    facebook: type
      .object(),
    token: type
      .string()
      .default(dbfuncs.generateUUID),
    roles: type
      .array()
      .schema(type
        .string()
        .enum('owner', 'admin', 'billing'))
      .default([]),
    status: type
      .string()
      .enum(['inactive', 'active', 'disabled'])
      .default('inactive'),
    notes: type
      .string(),
    created: type
      .date()
      .default(dbfuncs.generateDate),
    updated: type
      .date()
      .default(null)
  });
  User.pre('save', dbfuncs.setUpdatedDate);
  User.defineStatic('generateUUID', dbfuncs.generateUUID);

  var Account = god.model('account');
  User.belongsTo(Account, 'account', 'accountId', 'id');
})(module.parent.god);

