'use strict';

(function (god) {
  var log = god.log.child({utils: 'response'});

  exports.success = function success(obj, status) {
    var self   = this;
    status     = status || 200;
    var output = {success: true, result: obj};
    self.status(status).json(output);
  };

  exports.sendError = function sendError(code, obj) {
    var self   = this;
    code       = code || 'UNKNOWN_ERROR';
    var status = 500;
    var output = {
      success: false,
      code: code,
      message: 'An unknown error has occurred',
      error: obj
    };
    if (self.locales.hasOwnProperty(code)) {
      var msg        = self.locales[code];
      output.message = msg.message;
      status         = msg.code;
    } else {
      log.error('Invalid error code', code);
    }

    self.status(status).json(output);
  };

})(module.parent.god);