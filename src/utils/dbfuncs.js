'use strict';

(function (god) {
  var log  = god.log.child({utils: 'dbfuncs'});
  var uuid = require('uuid');

  exports.generateUUID = function generateUUID() {
    return uuid.v4();
  };

  exports.generateDate = function generateDate() {
    return new Date();
  };

  exports.setUpdatedDate = function setUpdatedDate(next) {
    this.updated = new Date();
    next();
  };
})(module.parent.god);