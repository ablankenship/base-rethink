'use strict';

(function (god) {
  var log      = god.log.child({utils: 'middleware'});
  var Response = god.util('response');
  var User = god.model('user');

  exports.hasRole = function hasRole(role) {
    function checkRole(req, res, next) {
      var user = req.authedUser;
      console.log(user);
      if (user && user.roles.indexOf(role !== -1)) {
        next();
      } else {
        res.sendError('MISSING_ROLE', {role: role});
      }
    }

    return checkRole;
  };

  exports.checkAuth = function checkAuth(req, res, next){
    //for testing
    req.authedUser = new User({});
    next();
  };

  exports.locales = function (req, res, next) {
    res.success   = Response.success;
    res.sendError = Response.sendError;

    if(god.errorCodes[req.locale]){
      res.locales = god.errorCodes[req.locale];
      next();
    } else {
      log.error('Unsupported locale', req.locale);
      res
        .status(500)
        .json({
          success: false,
          code: 'UNKNOWN_ERROR',
          message: 'An unknown error has occurred'
        });
    }
  };

})(module.parent.god);