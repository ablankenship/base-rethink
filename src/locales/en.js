var locale = {
  MISSING_ROLE: {
    code: 401,
    message: 'Missing required permissions to access this feature'
  },
  NOT_FOUND: {
    code: 404,
    message: 'The requested resource was not found'
  },
  UNKNOWN_ERROR: {
    code: 500,
    message: 'An unknown error has occurred'
  }
};

module.exports = locale;